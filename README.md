## Quickstart

1. Fetch repos into desired workspace
    ```
    ./setup_workspace.sh
    ```

2. Connect the camera and set the correct host IP address

3. Enable xhost

    ```
    xhost +
    ```

4. Start docker
    ```
    docker compose up
    ```