#!/bin/bash
set -e

CURRENT_DIR=$(pwd)
echo "CURRENT_DIR is '$CURRENT_DIR'"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo "SCRIPT_DIR is '$SCRIPT_DIR'"

source $SCRIPT_DIR/.env
echo "PROJECT_DIR is '$PROJECT_DIR'"

mkdir -p $PROJECT_DIR/src && cd $PROJECT_DIR

vcs import src < $SCRIPT_DIR/my.repos && cd $CURRENT_DIR